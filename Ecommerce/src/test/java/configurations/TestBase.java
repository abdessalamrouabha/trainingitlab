package configurations;


import constants.DriverEnum;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class TestBase {
    protected WebDriver driver; //
    private DriverEnum testType;
    //String getUrl = "http://automationpractice.com/index.php";
    String getUrl ="http://tutorialsninja.com/demo/";
    String Title = "Your Store";


    public TestBase(DriverEnum type) {
        this.testType = type;
    }


    @BeforeEach
    public void setup() throws Exception {
        driver = WebDriverManager.chromedriver().create();
        driver.get(getUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Assertions.assertThat(driver.getTitle()).isEqualTo(Title);

    }

    @AfterEach
    public void tearDown() {
       driver.close();
       driver.quit();
        driver = null;
    }

}

