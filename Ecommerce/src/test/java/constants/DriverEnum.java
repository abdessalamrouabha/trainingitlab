package constants;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public  enum DriverEnum {
    CHROME {
        @Override
        public WebDriver create() throws Exception {
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver();
        }
    },
    FIREFOX {
        @Override
        public WebDriver create() throws Exception {
            return new FirefoxDriver();
        }

    },
       EDGE {
            @Override
            public WebDriver create() throws Exception {
               WebDriverManager.edgedriver().setup();
                return new EdgeDriver();
            }

    };

    public abstract WebDriver create() throws Exception;


}