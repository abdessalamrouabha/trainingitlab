package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class BasePage {
    public WebDriver driver;
    public WebDriverWait wait;



    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = (WebDriverWait) new WebDriverWait(driver, Duration.ofSeconds(15))
                .ignoring(StaleElementReferenceException.class);
    }

    ExpectedCondition<Boolean> pageLoaded = driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");

    public void click(By locator) {
        try {
            waitForElement(locator);
            driver.findElement(locator).click();
        }
        catch (StaleElementReferenceException sef) {
            waitForElement(locator);
            driver.findElement(locator).click();
        }
    }

    public void fillString(By locator, String input) {
        waitForElement(locator);
        driver.findElement(locator).sendKeys(input);
    }



    public void waitForElement(By locator) {wait.until(ExpectedConditions.visibilityOfElementLocated(locator));}

    public void waitForPageToBeLoaded() {
        wait.until(pageLoaded);
    }


    public void validationText(By locator, String text) {
        waitForElement(locator);
        assertThat(driver.findElement(locator).getText()).contains(text);
    }

    public String getElementText(By locator) {
        return driver.findElement(locator).getText();
    }

}
