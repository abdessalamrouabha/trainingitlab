package pages.Home;

public class HomePageEnum {

    public String SIGNIN_BUTTON() {
        return "//a[contains(text(), 'Sign in')]";
    }

    public String SIGNOUT_BUTTON() {return "//a[contains(text(), 'Sign out')]";}

    public String Check_Box_Homme() {
        return "//*[@id='id_gender1']";
    }
    public String FirstName() {
        return "//input[@id='customer_firstname']";
    }
    public String Password() {
        return "//input[@id='passwd']";
    }
    public String LasttName() {
        return "//input[@id='customer_lastname']";
    }
    public String Adress() {return "//input[@id='address1']";}
}