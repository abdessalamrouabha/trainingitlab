package pages.Home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

public class HomePage extends BasePage {

    private HomePageEnum webEnum;

    public HomePage(WebDriver driver) {
        super(driver);
        this.webEnum = new HomePageEnum();
    }

    public static HomePage of(WebDriver driver) {
        return new HomePage(driver);
    }

    private By signInButton() {
        return By.xpath(webEnum.SIGNIN_BUTTON());
    }

    private By signOutButton() {
        return By.xpath(webEnum.SIGNOUT_BUTTON());
    }
    private By CheckBoxHomme() {
        return By.xpath(webEnum.Check_Box_Homme());
    }
    private By firstname() {
        return By.xpath(webEnum.FirstName());
    }
    private By lasttname() {
        return By.xpath(webEnum.LasttName());
    }
    private By password () {
        return By.xpath(webEnum.Password());
    }
    private By adress () {
        return By.xpath("//input[@id='address1']");
    }

    public HomePage WriteAdress (String adress) {
        fillString(adress(), adress);
        //waitForPageToBeLoaded();
        return this;

    }

    public HomePage WritePw (String pw) {
        fillString(password(),pw);
        //waitForPageToBeLoaded();
        return this;
    }

    public HomePage SignInPage() {
        click(signInButton());
        //waitForPageToBeLoaded();
        return this;
    }

    public HomePage signOut() {
        click(signOutButton());
        return this;
    }
    public HomePage CheckBoxH() {
        click(CheckBoxHomme());
        return this;
    }
    public HomePage WriteFirstName(String first) {
        fillString(firstname(), first);
        //waitForPageToBeLoaded();
        return this;
    }
    public HomePage WriteLaststName(String last) {
        fillString(lasttname(), last);
        //waitForPageToBeLoaded();
        return this;

    }

}
