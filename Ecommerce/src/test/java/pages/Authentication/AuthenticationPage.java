package pages.Authentication;


import com.codeborne.selenide.commands.Click;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

public class AuthenticationPage extends BasePage {

    private AuthenticationPageEnum webEnum;

    private AuthenticationPage(WebDriver driver) {
        super(driver);
        this.webEnum = new AuthenticationPageEnum();
    }

    public static AuthenticationPage of(WebDriver driver) {
        return new AuthenticationPage(driver);
    }


    //Locators


    //locators in section  Registered
    private By MyACCOUNT() {
        return By.xpath(webEnum.link_MyAccount());
    }

    private By Resgister() {
        return By.xpath(webEnum.Link_REGISTERED());
    }

    private By firstname() {
        return By.xpath(webEnum.FIRSNAME_INPUT_REGISTERED());
    }


    public AuthenticationPage ClickMyAccount() {
        click(MyACCOUNT());
        return this;

    }

    public AuthenticationPage ClickRegistred() {
        click(Resgister());
        return this;

    }

    public AuthenticationPage fillfirstname(String firstname) {
        fillString(firstname(), firstname);
        return this;
    }

}