package pages.Authentication;

public class AuthenticationPageEnum {
    // all locators for registred
    public String link_MyAccount() {
        return "//a[@title='My Account' and @class='dropdown-toggle']";
    }

    public String Link_REGISTERED(){return "//a[contains(text(),'Register')]";}

    public String FIRSNAME_INPUT_REGISTERED(){return "//input[@class='form-control' and @name='firstname']";}
    public String FORGOTTEN_PASSWORD_LINK(){return "/a[text()='Forgot your password?']";}
    public String SIGNIN_BUTTON(){return "//button[@name='SubmitLogin']";}
    public String EMAIL_INPUT_CREATEACCOUNT(){return "//div/input[@name='email_create']";}
    public String CREATEACCOUNT_BUTTON(){return "//div/button[@name='SubmitCreate']";}
    public String ERROR_CREATEACCOUNT_EMAIL(){return "//ol/li[text()='Invalid email address.']";}
    public String ERROR_REGISTERED_PASSWORD(){return "//ol/li[text()='Password is required.']";}
    public String ERROR_REGISTERED_EMAIL(){return "//ol/li[text()='An email address required.']";}

}
